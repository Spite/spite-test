((packager . "retropikzel")
 (name . "spite")
 (version . "v0-1-0")
 (description . "Remember to change this")
 (license . "LGPL")
 (dependencies
   ("local"
    (retropikzel pffi v0-1-0))
   ("local"
    (retropikzel event v0-1-0)
    (retropikzel time v0-1-0)
    (retropikzel color v0-1-1)
    (retropikzel geometry v0-1-0)
    (retropikzel vector-2d v0-2-0))))
