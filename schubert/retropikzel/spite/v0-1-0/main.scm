;> # Spite
;>
(define-library
  (retropikzel spite v0-1-0 main)
  (import (scheme base)
          (scheme write)
          (scheme complex)
          (scheme process-context)
          (scheme file)
          (scheme load)
          (retropikzel pffi v0-1-0 main)
          (retropikzel event v0-1-0 main)
          (retropikzel time v0-1-0 main)
          (retropikzel color v0-1-1 main)
          (retropikzel geometry v0-1-0 main)
          (retropikzel vector-2d v0-2-0 main))
  (export spite-init
          spite-start
          spite-started?
          event-make
          event-type-get
          event-data-get
          event-push
          event-clear
          spite-image-load
          spite-image-draw
          spite-image-draw-slice
          spite-point-draw
          spite-line-draw
          spite-rectangle-draw
          spite-rectangle-fill
          spite-font-load
          spite-text-draw
          spite-audio-load
          spite-audio-play
          spite-window-resizing-allow
          spite-window-resizing-disallow
          spite-renderer-size-set
          spite-reload-allow
          color-make
          color?
          color-red-get
          color-red-set!
          color-green-get
          color-green-set!
          color-blue-get
          color-blue-set!
          color-alpha-get
          color-alpha-set!
          color->vector
          time-as-milliseconds
          time-as-seconds
          time-interval
          time-timeout
          geometry-point-make
          geometry-point?
          geometry-point-x-get
          geometry-point-x-set!
          geometry-point-y-get
          geometry-point-y-set!
          geometry-point-inside-rectangle?
          geometry-line-make
          geometry-line?
          geometry-line-a-get
          geometry-line-a-set!
          geometry-line-b-get
          geometry-line-b-set!
          geometry-rectangle-make
          geometry-rectangle?
          geometry-rectangle-a-get
          geometry-rectangle-a-set!
          geometry-rectangle-b-get
          geometry-rectangle-b-set!
          geometry-rectangle-c-get
          geometry-rectangle-c-set!
          geometry-rectangle-d-get
          geometry-rectangle-d-set!
          geometry-rectangle-position-get
          geometry-rectangle-position-set!
          geometry-rectangle-x-get
          geometry-rectangle-y-get
          geometry-rectangle-width-get
          geometry-rectangle-height-get
          vector-2d-make
          vector-2d?
          vector-2d-width-get
          vector-2d-height-get
          vector-2d-width-set!
          vector-2d-height-set!
          vector-2d-cells-get
          vector-2d-cells-set!
          vector-2d-get
          vector-2d-set!
          vector-2d-for-each
          vector-2d-display)
  (begin

    ;; * at the end of name marks a pffi pointer in global scope

    (define version "v0-1-0")
    (define started? #f)
    (define exit? #f)
    (define error? #f)
    (define main-file-path "main.scm")
    (define reload-key #f)
    (define scale-x 1.0)
    (define scale-y 1.0)
    (define slash (cond-expand (windows (string #\\)) (else "/")))
    (define additional-load-paths
      (list (string-append "retropikzel"
                           slash
                           "spite"
                           slash
                           version)
            (string-append "schubert"
                           slash
                           "retropikzel"
                           slash
                           "spite"
                           slash
                           version)))
    (define sdl2* (pffi-shared-object-auto-load "SDL2/SDL.h" "SDL2" additional-load-paths))
    (define sdl2-image* (pffi-shared-object-auto-load "SDL2/SDL_image.h" "SDL2_image" additional-load-paths))
    (define sdl2-ttf* (pffi-shared-object-auto-load "SDL2/SDL_ttf.h" "SDL2_ttf" additional-load-paths))
    (define sdl2-mixer* (pffi-shared-object-auto-load "SDL2/SDL_mixer.h" "SDL2_mixer" additional-load-paths))
    (pffi-define sdl-init sdl2* 'SDL_Init 'int '(int))
    (pffi-define sdl-ttf-init sdl2-ttf* 'TTF_Init 'int '())
    (pffi-define sdl-mix-open-audio sdl2-mixer* 'Mix_OpenAudio 'int (list 'int 'int 'int 'int))
    (pffi-define sdl-create-window sdl2* 'SDL_CreateWindow 'pointer (list 'pointer 'int 'int 'int 'int 'int))
    (pffi-define sdl-create-renderer sdl2* 'SDL_CreateRenderer 'pointer (list 'pointer 'int 'int))
    (pffi-define sdl-render-setlogial-size sdl2* 'SDL_RenderSetLogicalSize 'int (list 'pointer 'int 'int))
    (pffi-define sdl-render-set-integer-scale sdl2* 'SDL_RenderSetIntegerScale 'int (list 'pointer 'int))
    (pffi-define sdl-set-render-draw-color sdl2* 'SDL_SetRenderDrawColor 'int (list 'pointer 'int 'int 'int 'int))
    (pffi-define sdl-render-clear sdl2* 'SDL_RenderClear 'int (list 'pointer ))
    (pffi-define sdl-render-present sdl2* 'SDL_RenderPresent 'int (list 'pointer ))
    (pffi-define sdl-get-key-from-scancode sdl2* 'SDL_GetKeyFromScancode 'int32 (list 'int))
    (pffi-define sdl-get-key-name sdl2* 'SDL_GetKeyName 'string (list 'int32))
    (pffi-define sdl-poll-event sdl2* 'SDL_PollEvent 'int (list 'pointer))
    (pffi-define sdl-img-load-texture sdl2-image* 'IMG_LoadTexture 'pointer (list 'pointer 'pointer))
    (pffi-define sdl-render-copy sdl2* 'SDL_RenderCopy 'int (list 'pointer 'pointer 'pointer 'pointer))
    (pffi-define sdl-render-draw-line sdl2* 'SDL_RenderDrawLine 'int (list 'pointer 'int 'int 'int 'int))
    (pffi-define sdl-render-set-scale sdl2* 'SDL_RenderSetScale 'int (list 'pointer 'float 'float))
    (pffi-define sdl-render-fill-rect sdl2* 'SDL_RenderFillRect 'int (list 'pointer 'pointer))
    (pffi-define sdl-ttf-open-font sdl2-ttf* 'TTF_OpenFont 'pointer (list 'pointer 'int))
    (pffi-define sdl-ttf-render-utf8-solid sdl2-ttf* 'TTF_RenderUTF8_Solid 'pointer (list 'pointer 'pointer 'pointer))
    (pffi-define sdl-create-texture-from-surface sdl2* 'SDL_CreateTextureFromSurface 'pointer (list 'pointer 'pointer))
    (pffi-define sdl-query-texture sdl2* 'SDL_QueryTexture 'int (list 'pointer 'pointer 'pointer 'pointer 'pointer))
    (pffi-define sdl-mix-load-wav sdl2-mixer* 'Mix_LoadWAV 'pointer (list 'pointer))
    (pffi-define sdl-mix-play-channel sdl2-mixer* 'Mix_PlayChannel 'int (list 'int 'pointer 'int))
    (pffi-define sdl-set-window-resizable sdl2* 'SDL_SetWindowResizable 'void (list 'pointer 'int))
    (pffi-define sdl-render-get-scale sdl2* 'SDL_RenderGetScale 'void (list 'pointer 'pointer))


    (define window* #f)
    (define renderer* #f)
    (define event* (pffi-pointer-allocate 4000))
    (define draw-rect* (pffi-pointer-allocate (* (pffi-size-of 'int) 4)))
    (define draw-slice-rect* (pffi-pointer-allocate (* (pffi-size-of 'int) 4)))
    (define draw-text-text-width* (pffi-pointer-allocate (pffi-size-of 'int)))
    (define draw-text-text-height* (pffi-pointer-allocate (pffi-size-of 'int)))
    (define draw-text-color* (pffi-pointer-allocate (* (pffi-size-of 'uint8) 125)))
    (define null* (pffi-pointer-null))

    (define update-procedure #f)
    (define draw-procedure #f)

    (define images (vector))
    (define fonts (vector))
    (define audios (vector))

    ;> ## Events
    ;>

    ;> ### key-down
    ;>
    ;> Arguments:
    ;>
    ;> - key (string)
    ;>   - The name of the pressed key as a string
    ;>   - scancode (number)
    ;>     - The scancode of the pressed key
    ;> - repeat? (boolean)
    ;>   - If the key press is a repeat (It's hold down)
    ;>

    ;> ### key-up
    ;>
    ;> Arguments:
    ;>
    ;> - key (string)
    ;>   - The name of the pressed key as a string
    ;>   - scancode (number)
    ;>     - The scancode of the pressed key
    ;> - repeat? (boolean)
    ;>   - If the key press is a repeat (It's hold down)
    ;>

    ;> ### mouse-moved
    ;>
    ;> Arguments:
    ;>
    ;> - x (number)
    ;>   - X position of the mouse inside the window
    ;> - y (number)
    ;>   - Y position of the mouse inside the window
    ;>

    ;> ### mouse-button-up
    ;>
    ;> Arguments:
    ;>
    ;> - x (number)
    ;>   - X position of the mouse inside the window
    ;> - y (number)
    ;>   - Y position of the mouse inside the window
    ;> - button (number)
    ;>   - The index of a button that went down
    ;> - clicks
    ;>   - The count of clicks, > 1 means doubleclick for example
    ;>

    ;> ### mouse-button-down
    ;>
    ;> Arguments:
    ;>
    ;> - x (number)
    ;>   - X position of the mouse inside the window
    ;> - y (number)
    ;>   - Y position of the mouse inside the window
    ;> - button (number)
    ;>   - The index of a button that went down
    ;> - clicks
    ;>   - The count of clicks, > 1 means doubleclick for example
    ;>

    ;> ## Procedures
    ;>

    (define main-loop
      (lambda ()
        ;(call-with-current-continuation
        ;(lambda (k)
        ;(if (not error?)
        ;(with-exception-handler
        ;(lambda (x)
        ;(cond-expand
        ;(kawa
        ;(write x)
        ;(newline)
        ;(invoke x 'printStackTrace (current-error-port)))
        ;(else
        ;(display "Error: ")
        ;(newline)
        ;(write (error-object-message x))
        ;(newline)
        ;(write (error-object-irritants x))
        ;(newline)
        ;(write x)
        ;(newline)))
        ;(set! error? #t)
        ;(k))
        ;(lambda ()
        (sdl2-events-get)
        (time-update)
        (update-procedure (event-poll))
        (render-clear)
        (draw-procedure)
        (render-present)
        ;))
        ;(sdl2-events-get))))
        (if (not exit?) (main-loop))))

    (define sdl2-event->spite-event
      (lambda (event)
        (let ((type (pffi-pointer-get event 'uint32 0)))
          (cond
            ((= type 256)
             (let ((type 'quit))
               (list (cons 'type type))))
            ((or (= type 768) (= type 769))
             (let*
               ((type (if (= type 768) 'key-down 'key-up))
                (scancode (pffi-pointer-get event
                                            'int
                                            (+ (* (pffi-size-of 'uint32) 3)
                                               (* (pffi-size-of 'uint8) 4))))
                (keycode (sdl-get-key-from-scancode scancode))
                (key (pffi-pointer->string
                       (sdl-get-key-name keycode)))
                (repeat? (= (pffi-pointer-get
                              event
                              'uint8
                              (+ (* (pffi-size-of 'uint32) 3)
                                 (pffi-size-of 'uint8))) 1)))
               (list (cons 'type type)
                     (cons 'key key)
                     (cons 'scancode scancode)
                     (cons 'repeat? repeat?))))
            ((= type 1024)
             (let ((type 'mouse-motion)
                   (x (pffi-pointer-get event 'int32 (+ (* (pffi-size-of 'uint32) 5))))
                   (y (pffi-pointer-get event 'int32 (+ (* (pffi-size-of 'uint32) 6)))))
               (list (cons 'type type)
                     (cons 'x x)
                     (cons 'y y))))
            ((or (= type 1025) (= type 1026))
             (let ((type (if (= type 1025) 'mouse-button-down 'mouse-button-up))
                   (x (pffi-pointer-get event 'int32 (+ (* (pffi-size-of 'uint32) 4)
                                                        (* (pffi-size-of 'uint8) 4))))
                   (y (pffi-pointer-get event 'int32 (+ (* (pffi-size-of 'uint32) 4)
                                                        (* (pffi-size-of 'uint8) 4)
                                                        (pffi-size-of 'int32))))
                   (button (pffi-pointer-get event 'uint8 (+ (* (pffi-size-of 'uint32) 4))))
                   (clicks (pffi-pointer-get event 'uint8 (+ (* (pffi-size-of 'uint32) 4)
                                                             (* (pffi-size-of 'uint8) 2)))))
               (list (cons 'type type)
                     (cons 'x x)
                     (cons 'y y)
                     (cons 'button button)
                     (cons 'clicks clicks))))
            (else
              (list (cons 'type 'sdl2-event)
                    (cons 'sdl2-type-number type)))))))

    (define sdl2-events-get
      (lambda ()
        (let ((poll-result (sdl-poll-event event*)))
          (cond
            ((= poll-result 1)
             (let ((event (sdl2-event->spite-event event*)))

               (cond ((and reload-key
                           (equal? (cdr (assoc 'type event)) 'key-down)
                           (string=? (cdr (assoc 'key event)) reload-key))
                      (set! error? #f)
                      (display "Reloading!")
                      (newline)
                      (spite-reload main-file-path))
                     ((equal? (cdr (assoc 'type event)) 'quit) (set! exit? #t)))
               (event-push (event-make (cdr (assoc 'type event)) event))
               (sdl2-events-get))))

          )))


    (define render-clear
      (lambda ()
        (sdl-set-render-draw-color renderer* 255 255 255 255)
        (sdl-render-clear renderer*)))

    (define render-present
      (lambda ()
        (sdl-render-present renderer*)))

    ;> ### spite-image-load
    ;>
    ;> Arguments:
    ;>
    ;> - path (string)
    ;>   - Path to the image you want to load
    ;>
    ;> Returns: (number) Image index
    ;>
    ;> Loads image from the path, supported filetypes are same as supported by
    ;> SDL_image [https://wiki.libsdl.org/SDL2_image/FrontPage](https://wiki.libsdl.org/SDL2_image/FrontPage)
    (define spite-image-load
      (lambda (path)
        (if (not (string? path)) (error "Load path must be string" path))
        (if (not (file-exists? path)) (error (string-append "Could not load image, no such file: " path)))
        (set! images
          (vector-append
            images
            (vector (sdl-img-load-texture renderer* (pffi-string->pointer path)))))
        (- (vector-length images) 1)))

    ;> ### spite-image-draw
    ;>
    ;> Arguments:
    ;>
    ;> - image-index (number)
    ;>   - Index returned by spite-image-load
    ;> - x (number)
    ;>   - Images left top corner x position inside the window
    ;> - y (number)
    ;>   - Images left top corner y position inside the window
    ;> - width (number)
    ;>   - What width should the image be drawn
    ;> - height (number)
    ;>   - What height should the image be drawn
    (define spite-image-draw
      (lambda (image-index x y width height)
        (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 0) x)
        (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 1) y)
        (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 2) width)
        (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 3) height)
        (sdl-render-copy renderer* (vector-ref images image-index) null* draw-rect*)))

    ;> ### spite-image-draw-slice
    ;>
    ;> Arguments:
    ;>
    ;> - image-index (number)
    ;>   - Index returned by spite-image-load
    ;> - x (number)
    ;>   - Images left top corner x position inside the window
    ;> - y (number)
    ;>   - Images left top corner y position inside the window
    ;> - width (number)
    ;>   - What width should the slice be drawn
    ;> - height (number)
    ;>   - What height should the slice be drawn
    ;> - slice-x (number)
    ;>   - Slices top left corner x position inside the image
    ;> - slice-y (number)
    ;>   - Slices top left corner y position inside the image
    ;> - slice-height
    ;>   - Slices height
    ;> - slice-width
    ;>   - Slices width
    ;>
    ;> Slice here refers to rectangle inside the image. So instead of drawing
    ;> the whole image, only the slice is drawn
    (define spite-image-draw-slice
      (lambda (image x y width height slice-x slice-y slice-height slice-width)
        (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 0) x)
        (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 1) y)
        (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 2) width)
        (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 3) height)
        (pffi-pointer-set! draw-slice-rect* 'int (* (pffi-size-of 'int) 0) slice-x)
        (pffi-pointer-set! draw-slice-rect* 'int (* (pffi-size-of 'int) 1) slice-y)
        (pffi-pointer-set! draw-slice-rect* 'int (* (pffi-size-of 'int) 2) slice-width)
        (pffi-pointer-set! draw-slice-rect* 'int (* (pffi-size-of 'int) 3) slice-height)
        (sdl-render-copy renderer* (vector-ref images image) draw-slice-rect* draw-rect*)))

    ;> ### spite-point-draw
    ;> Arguments:
    ;> - point (geometry-point) Point you want to draw
    ;> - size (number) How big the point should be
    ;> - color (color) The color of the point
    (define spite-point-draw
      (lambda (point size color)
        (let ((x (geometry-point-x-get point))
              (y (geometry-point-y-get point)))
          (sdl-set-render-draw-color renderer*
                                     (color-red-get color)
                                     (color-green-get color)
                                     (color-blue-get color)
                                     (color-alpha-get color))
          (spite-renderer-scale-set size size)
          (sdl-render-draw-line renderer*
                                (exact (round (/ x size)))
                                (exact (round (/ y size)))
                                (exact (round (/ x size)))
                                (exact (round (/ y size))))
          (spite-renderer-scale-set scale-x scale-y))))

    ;> ### spite-line-draw
    ;> Arguments:
    ;> - line (geometry-line) Line you want to draw
    ;> - size (number) How big the line should be
    ;> - color (color) The color of the line
    (define spite-line-draw
      (lambda (line size color)
        (let ((x1 (geometry-point-x-get (geometry-line-a-get line)))
              (y1 (geometry-point-y-get (geometry-line-a-get line)))
              (x2 (geometry-point-x-get (geometry-line-b-get line)))
              (y2 (geometry-point-y-get (geometry-line-b-get line))))
          (sdl-set-render-draw-color renderer*
                                     (color-red-get color)
                                     (color-green-get color)
                                     (color-blue-get color)
                                     (color-alpha-get color))
          (sdl-render-set-scale renderer* (inexact (/ size 1)) (inexact (/ size 1)))
          (sdl-render-draw-line renderer*
                                (exact (round (/ x1 size)))
                                (exact (round (/ y1 size)))
                                (exact (round (/ x2 size)))
                                (exact (round (/ y2 size))))
          (sdl-render-set-scale renderer* scale-x scale-y))))

    ;> ### spite-rectangle-draw
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle)
    ;> - size (number) The size of line used to draw the rectangle
    ;> - color (color)
    (define spite-rectangle-draw
      (lambda (rectangle size color)
        (let ((points (list (geometry-rectangle-b-get rectangle)
                            (geometry-rectangle-c-get rectangle)
                            (geometry-rectangle-d-get rectangle)))
              (start-point (geometry-rectangle-a-get rectangle))
              (end-point (geometry-rectangle-d-get rectangle))
              (previous-point (geometry-rectangle-a-get rectangle)))
          (sdl-set-render-draw-color renderer*
                                     (color-red-get color)
                                     (color-green-get color)
                                     (color-blue-get color)
                                     (color-alpha-get color))
          (sdl-render-set-scale renderer* (inexact (/ size 1)) (inexact (/ size 1)))
          (for-each
            (lambda (point)
              (sdl-render-draw-line renderer*
                                    (exact (round (/ (geometry-point-x-get previous-point) size)))
                                    (exact (round (/ (geometry-point-y-get previous-point) size)))
                                    (exact (round (/ (geometry-point-x-get point) size)))
                                    (exact (round (/ (geometry-point-y-get point) size))))
              (set! previous-point point))
            points)
          (sdl-render-draw-line renderer*
                                (exact (round (/ (geometry-point-x-get end-point) size)))
                                (exact (round (/ (geometry-point-y-get end-point) size)))
                                (exact (round (/ (geometry-point-x-get start-point) size)))
                                (exact (round (/ (geometry-point-y-get start-point) size))))
          (sdl-render-set-scale renderer* scale-x scale-y))))

    ;> ### spite-rectangle-fill
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle)
    ;> - color (color)
    (define spite-rectangle-fill
      (lambda (rectangle color)
        (pffi-pointer-set! draw-rect*
                           'int
                           (* (pffi-size-of 'int) 0)
                           (geometry-point-x-get (geometry-rectangle-a-get rectangle)))
        (pffi-pointer-set! draw-rect*
                           'int
                           (* (pffi-size-of 'int) 1)
                           (geometry-point-y-get (geometry-rectangle-a-get rectangle)))
        (pffi-pointer-set! draw-rect*
                           'int
                           (* (pffi-size-of 'int) 2)
                           (geometry-rectangle-width-get rectangle))
        (pffi-pointer-set! draw-rect*
                           'int
                           (* (pffi-size-of 'int) 3)
                           (geometry-rectangle-height-get rectangle))
        (sdl-set-render-draw-color renderer*
                                   (color-red-get color)
                                   (color-green-get color)
                                   (color-blue-get color)
                                   (color-alpha-get color))
        (sdl-render-fill-rect renderer* draw-rect*)
        (sdl-set-render-draw-color renderer* 255 255 255 255)))

    ;> ### spite-font-load
    ;>
    ;> Arguments:
    ;>
    ;> - path (string)
    ;>   - Path to the font file you want to load
    ;> - size (number)
    ;>   - The size the font should be
    ;>
    ;> Returns: (number) Index of the loaded font
    ;>
    ;> Supports any font format SDL_format supports
    ;> [https://wiki.libsdl.org/SDL2_ttf/FrontPage](https://wiki.libsdl.org/SDL2_ttf/FrontPage)
    (define spite-font-load
      (lambda (path size)
        (if (not (string? path)) (error "Load path must be string" path))
        (if (not (file-exists? path)) (error (string-append "Could not load font, no such file: " path)))
        (set! fonts
          (vector-append
            fonts
            (vector (sdl-ttf-open-font (pffi-string->pointer path) size))))
        (- (vector-length fonts) 1)))

    ;> ### spite-text-draw
    ;>
    ;> Arguments:
    ;>
    ;> - text (string)
    ;>   - Text you want to draw
    ;> - x (number)
    ;>   - Left top corner x position of the text inside the window
    ;> - y (number)
    ;>   - Left top corner y position of the text inside the window
    ;> - font (number)
    ;>   - Font index returned by spite-font-load
    ;> - color (vector (number) (number) (number))
    ;>   - Vector consisting of three numbers red, green and blue between 0-255
    (define spite-text-draw
      (lambda (text x y font color)
        (pffi-pointer-set! draw-text-color* 'uint8 (* (pffi-size-of 'uint8) 0) (color-red-get color))
        (pffi-pointer-set! draw-text-color* 'uint8 (* (pffi-size-of 'uint8) 1) (color-green-get color))
        (pffi-pointer-set! draw-text-color* 'uint8 (* (pffi-size-of 'uint8) 2) (color-blue-get color))
        (pffi-pointer-set! draw-text-color* 'uint8 (* (pffi-size-of 'uint8) 3) (color-alpha-get color))
        (let* ((surface (sdl-ttf-render-utf8-solid (vector-ref fonts font)
                                                   (pffi-string->pointer (string-copy text))
                                                   (pffi-pointer-deref draw-text-color*)))
               (texture (sdl-create-texture-from-surface renderer* surface)))
          (sdl-query-texture texture null* null* draw-text-text-width* draw-text-text-height*)
          (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 0) x)
          (pffi-pointer-set! draw-rect* 'int (* (pffi-size-of 'int) 1) x)
          (pffi-pointer-set! draw-rect*
                             'int
                             (* (pffi-size-of 'int) 2)
                             (pffi-pointer-get draw-text-text-width* 'int 0))
          (pffi-pointer-set! draw-rect*
                             'int
                             (* (pffi-size-of 'int) 3)
                             (pffi-pointer-get draw-text-text-height* 'int 0))
          (sdl-render-copy renderer* texture null* draw-rect*))))

    ;> ### spite-audio-load
    ;>
    ;> Arguments:
    ;>
    ;> - path (string)
    ;>  - Path to the audio file
    ;>
    ;> Returns: (number) Index of loaded audio
    ;>
    ;> Supports any format supported by
    ;> [https://www.libsdl.org/projects/old/SDL_mixer/](https://www.libsdl.org/projects/old/SDL_mixer/)
    (define spite-audio-load
      (lambda (path)
        (if (not (string? path)) (error "Load path must be string" path))
        (if (not (file-exists? path)) (error (string-append "Could not load audio, no such file: " path)))
        (set! audios
          (vector-append
            audios
            (vector (sdl-mix-load-wav (pffi-string->pointer path)))))
        (- (vector-length audios) 1)))

    ;> ### spite-audio-play
    ;>
    ;> Arguments:
    ;>
    ;> - audio-index (number)
    ;>  - Index of the audio returned by spite-audio-load
    (define spite-audio-play
      (lambda (audio-index)
        (sdl-mix-play-channel -1 (vector-ref audios audio-index) 0)))

    ;> ### spite-window-resizing-allow
    ;>
    ;> By default resizing is not allowed
    (define spite-window-resizing-allow
      (lambda ()
        (sdl-set-window-resizable window* 1)))

    ;> ### spite-window-resizing-disallow
    ;>
    ;> By default resizing is not allowed
    (define spite-window-resizing-disallow
      (lambda ()
        (sdl-set-window-resizable window* 0)))

    ;> ### spite-renderer-size-set
    ;>
    ;> Arguments:
    ;> - width (number) Set the renderer width
    ;> - height (number) Set the renderer height
    (define spite-renderer-size-set
      (lambda (width height)
        (sdl-render-setlogial-size renderer* width height)))

    ;> ### spite-renderer-scale-get
    ;>
    ;> Returns:
    ;>
    ;> - (list (cons 'x (number)) (cons 'y (number)))
    (define spite-renderer-scale-get
      (lambda ()
        (let ((x (pffi-pointer-allocate (pffi-size-of 'float)))
              (y (pffi-pointer-allocate (pffi-size-of 'float))))
          (sdl-render-get-scale x y)
          (list (cons 'x (pffi-pointer-get x 'float 0))
                (cons 'y (pffi-pointer-get y 'float 0))))))

    ;> ### spite-renderer-scale-set
    ;>
    ;> Arguments:
    ;>
    ;> - x (number)
    ;> - y (number)
    (define spite-renderer-scale-set
      (lambda (x y)
        (sdl-render-set-scale (inexact (/ x 1)) (inexact (/ y 1)))))


    ;> ### spite-start
    ;>
    ;> Starts the game
    (define spite-start
      (lambda (new-update-procedure new-draw-procedure)
        (set! update-procedure new-update-procedure)
        (set! draw-procedure new-draw-procedure)
        (cond
          ((not (spite-started?))
           (set! started? #t)
           (main-loop)))))

    ;> ### spite-reload-allow
    ;>
    ;> Arguments:
    ;> - key (string) The key you want to use for reload
    ;> - path (string) The path to your main file that you want to reload
    (define spite-reload-allow
      (lambda (key path)
        (set! reload-key key)
        (set! main-file-path path)))


    ;> ### spite-init
    ;>
    ;> Arguments:
    ;>
    ;> - title (string)
    ;>   - The window title
    ;> - widht (number)
    ;>   - Width of the window
    ;> - height
    ;>  - Height of the window
    ;>
    ;> Inits Spite and opens a window
    (define spite-init
      (lambda (title width height)
        (cond
          ((not (spite-started?))
           (sdl-init 32)
           (sdl-ttf-init)
           (sdl-mix-open-audio 44100 32784 2 2048)
           (set! window*
             (sdl-create-window (pffi-string->pointer title) 0 0 width height 4))
           (set! renderer* (sdl-create-renderer window* -1 2))
           (sdl-render-setlogial-size renderer* width height)
           (sdl-render-set-integer-scale renderer* 1)
           (render-clear)
           (render-present)))))

    ;> ### spite-started?
    ;>
    ;> Returns:
    ;> - (boolean) Returns #t if game is started
    (define spite-started?
      (lambda ()
        started?))

    ;> ### spite-reload
    ;>
    ;> Arguments:
    ;> - (string) Path to the main .scm file you want to reload
    (define spite-reload
      (lambda (main-file-path)
        (set! error? #f)
        (load main-file-path)
        (event-push (event-make 'reload (list)))))))
