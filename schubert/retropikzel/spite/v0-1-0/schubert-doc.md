# Spite



## Events



### key-down

Arguments:

- key (string)
  - The name of the pressed key as a string
  - scancode (number)
    - The scancode of the pressed key
- repeat? (boolean)
  - If the key press is a repeat (It's hold down)



### key-up

Arguments:

- key (string)
  - The name of the pressed key as a string
  - scancode (number)
    - The scancode of the pressed key
- repeat? (boolean)
  - If the key press is a repeat (It's hold down)



### mouse-moved

Arguments:

- x (number)
  - X position of the mouse inside the window
- y (number)
  - Y position of the mouse inside the window



### mouse-button-up

Arguments:

- x (number)
  - X position of the mouse inside the window
- y (number)
  - Y position of the mouse inside the window
- button (number)
  - The index of a button that went down
- clicks
  - The count of clicks, > 1 means doubleclick for example



### mouse-button-down

Arguments:

- x (number)
  - X position of the mouse inside the window
- y (number)
  - Y position of the mouse inside the window
- button (number)
  - The index of a button that went down
- clicks
  - The count of clicks, > 1 means doubleclick for example



## Procedures



### spite-update

Arguments:

- proc (procedure)
  - Procedure to run on the games main loop before drawing

Example:

(spite-update (lambda () (display "Updating...") (newline))


### spite-draw

Arguments:

- proc (procedure)
  - Procedure inside which all games drawing should happen

Example:

(spite-draw (lambda () (display "Drawing...") (newline)))


### spite-image-load

Arguments:

- path (string)
  - Path to the image you want to load

Returns: (number) Image index

Loads image from the path, supported filetypes are same as supported by
SDL_image [https://wiki.libsdl.org/SDL2_image/FrontPage](https://wiki.libsdl.org/SDL2_image/FrontPage)


### spite-image-draw

Arguments:

- image-index (number)
  - Index returned by spite-image-load
- x (number)
  - Images left top corner x position inside the window
- y (number)
  - Images left top corner y position inside the window
- width (number)
  - What width should the image be drawn
- height (number)
  - What height should the image be drawn


### spite-image-draw-slice

Arguments:

- image-index (number)
  - Index returned by spite-image-load
- x (number)
  - Images left top corner x position inside the window
- y (number)
  - Images left top corner y position inside the window
- width (number)
  - What width should the slice be drawn
- height (number)
  - What height should the slice be drawn
- slice-x (number)
  - Slices top left corner x position inside the image
- slice-y (number)
  - Slices top left corner y position inside the image
- slice-height
  - Slices height
- slice-width
  - Slices width

Slice here refers to rectangle inside the image. So instead of drawing
the whole image, only the slice is drawn


### spite-point-draw
Arguments:
- point (geometry-point) Point you want to draw
- size (number) How big the point should be
- color (color) The color of the point


### spite-line-draw
Arguments:
- line (geometry-line) Line you want to draw
- size (number) How big the line should be
- color (color) The color of the line


### spite-rectangle-draw

Arguments:
- rectangle (geometry-rectangle)
- size (number) The size of line used to draw the rectangle
- color (color)


### spite-font-load

Arguments:

- path (string)
  - Path to the font file you want to load
- size (number)
  - The size the font should be

Returns: (number) Index of the loaded font

Supports any font format SDL_format supports
[https://wiki.libsdl.org/SDL2_ttf/FrontPage](https://wiki.libsdl.org/SDL2_ttf/FrontPage)


### spite-text-draw

Arguments:

- text (string)
  - Text you want to draw
- x (number)
  - Left top corner x position of the text inside the window
- y (number)
  - Left top corner y position of the text inside the window
- font (number)
  - Font index returned by spite-font-load
- color (vector (number) (number) (number))
  - Vector consisting of three numbers red, green and blue between 0-255


### spite-audio-load

Arguments:

- path (string)
 - Path to the audio file

Returns: (number) Index of loaded audio

Supports any format supported by
[https://www.libsdl.org/projects/old/SDL_mixer/](https://www.libsdl.org/projects/old/SDL_mixer/)


### spite-audio-play

Arguments:

- audio-index (number)
 - Index of the audio returned by spite-audio-load


### spite-window-resizing-allow

By default resizing is not allowed


### spite-window-resizing-disallow

By default resizing is not allowed


### spite-renderer-size-set

Arguments:
- width (number) Set the renderer width
- height (number) Set the renderer height


### spite-renderer-scale-get

Returns:

- (list (cons 'x (number)) (cons 'y (number)))


### spite-renderer-scale-set

Arguments:

- x (number)
- y (number)


### spite-start

Starts the game


### spite-reload-allow

Arguments:
- key (string) The key you want to use for reload
- path (string) The path to your main file that you want to reload


### spite-init

Arguments:

- title (string)
  - The window title
- widht (number)
  - Width of the window
- height
 - Height of the window

Inits Spite and opens a window


### spite-started?

Returns:
- (boolean) Returns #t if game is started


### spite-reload

Arguments:
- (string) Path to the main .scm file you want to reload


