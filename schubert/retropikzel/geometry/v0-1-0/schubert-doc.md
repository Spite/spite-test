# Geometry
Library for 2D geometry utilities


## Records


### geometry-point
Fields:
- x (number) X position of the point
- y (number) Y position of the point


### geometry-rectangle
Fields:
- a (geometry-point) Top left corner when rectangle is made
- b (geometry-point) Top right corner when rectangle is made
- c (geometry-point) Bottom left corner when rectangle is made
- d (geometry-point) Bottom right corner when rectangle is made


## Procedures


### geometry-point-make
Arguments:
- x (number) X position of the point
- y (number) Y position of the point

Returns:
- (geometry-point)


### geometry-point?
Arguments:
- object (object) Any object
Returns:
- (boolean) Returns #t if object is geometry-point, otherwise #f


### geometry-point-x-get
Arguments:
- point (geometry-point) The point you want the x position of
Returns:
- (number) The x position of the point


### geometry-point-y-get
Arguments:
- point (geometry-point) The point you want the y position of
Returns:
- (number) The y position of the point


### geometry-rectangle-make
Arguments:
- a (geometry-point) Top left corner of rectangle
- b (geometry-point) Top right corner of rectangle
- c (geometry-point) Bottom left corner of rectangle
- d (geometry-point) Bottom right corner of rectangle
Returns:
- (geometry-rectangle)


### geometry-rectangle?
Arguments:
- object (object) Any object
Returns:
- (boolean) Returns #t if object is geometry-rectangle, otherwise #f


### geometry-rectangle-a-get
Arguments:
- rectangle (geometry-rectangle) Rectangle you want the corner of

Returns:
- (geometry-point) The a point of rectangle


### geometry-rectangle-b-get
Arguments:
- rectangle (geometry-rectangle) Rectangle you want the corner of

Returns:
- (geometry-point) The b point of rectangle


### geometry-rectangle-c-get
Arguments:
- rectangle (geometry-rectangle) Rectangle you want the corner of

Returns:
- (geometry-point) The c point of rectangle


### geometry-rectangle-d-get
Arguments:
- rectangle (geometry-rectangle) Rectangle you want the corner of

Returns:
- (geometry-point) The d point of rectangle


### geometry-point-inside-rectangle
Arguments:
- point (geometry-point) The point you want to check if it is inside rectangle
- rectangle (geometry-rectangle) The rectangle you want to check if the point is in

Returns:
- (boolean) If given point is inside given rectangle returns #t, othwerwise #f


### geometry-rectangle-position-get
Arguments:
- rectangle (geometry-rectangle) The rectangle you want position of

Returns:
- (geometry-point) Left top corner of rectangle


### geometry-rectangle-x-get
Arguments:
- rectangle (geometry-rectangle) The rectangle you want x position of

Returns:
- (number) X position of the rectangles top left corner


### geometry-rectangle-y-get
Arguments:
- rectangle (geometry-rectangle) The rectangle you want y position of

Returns:
- (number) Y position of the rectangles top left corner


### geometry-rectangle-width-get
Arguments:
- rectangle (geometry-rectangle) The rectangle you want width of

Returns:
- (number) Width of the rectangle


### geometry-rectangle-height-get
Arguments:
- rectangle (geometry-rectangle) The rectangle you want height of

Returns:
- (number) Height of the rectangle


### geometry-rectangle-position-set!
Arguments:
- rectangle (geometry-rectangle) The rectanle which position you want to set
- x (number) New x position for the top left corner
- y (number) New y position for the top left corner


