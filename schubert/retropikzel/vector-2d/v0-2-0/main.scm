;> # vector-2d

(define-library
  (retropikzel vector-2d v0-2-0 main)
  (import (scheme base)
          (scheme write))
  (export vector-2d-make
          vector-2d?
          vector-2d-width-get
          vector-2d-width-set!
          vector-2d-height-get
          vector-2d-height-set!
          vector-2d-cells-get
          vector-2d-cells-set!
          vector-2d-get
          vector-2d-set!
          vector-2d-for-each
          vector-2d-display)
  (begin

    ;> ## Records

    ;> ### vector-2d
    ;>
    ;> Fields:
    ;> - width (number)
    ;> - height (number)
    (define-record-type vector-2d
      (vector-2d-make-internal width height cells)
      vector-2d?
      (width vector-2d-width-get vector-2d-width-set!)
      (height vector-2d-height-get vector-2d-height-set!)
      (cells vector-2d-cells-get vector-2d-cells-set!))

    ;> ## Procedures

    ;> ### vector-2d?
    ;>
    ;> Arguments:
    ;> - object (object)
    ;>
    ;> Returns
    ;> - (boolean) Returns #t if object is vector-2d

    ;> ### vector-2d-make
    ;>
    ;> Arguments:
    ;> - width (number)
    ;> - height (number)
    ;> - fill (object) Fill the 2d vector with this value
    (define vector-2d-make
      (lambda (width height fill)
        (let ((cells (make-vector height))
              (index 0))
          (vector-for-each
            (lambda (_)
              (vector-set! cells index (make-vector width fill))
              (set! index (+ index 1)))
            cells)
          (vector-2d-make-internal width height cells))))

    ;> ### vector-2d-get
    ;>
    ;> Arguments:
    ;> - v2d (vector-2d)
    ;> - x (number) The x position of the cell
    ;> - y (number) The y position of the cell
    ;>
    ;> Returns
    ;> - (object) The contents of cell in position x y
    (define vector-2d-get
      (lambda (v2d x y)
        (cond
          ((>= x (vector-2d-width-get v2d))
           (error
             (string-append "[vector-2d] x: "
                            (number->string x)
                            " is out of bounds."
                            " Width is only "
                            (number->string (vector-2d-width-get v2d)))
             v2d))
          ((>= y (vector-2d-height-get v2d))
           (error (string-append "[vector-2d] y: "
                                 (number->string y)
                                 " is out of bounds."

                                 " Height is only "
                                 (number->string (vector-2d-height-get v2d)))
                  v2d)))
        (vector-ref (vector-ref (vector-2d-cells-get v2d) y) x)))

    ;> ### vector-2d-set!
    ;>
    ;> Arguments:
    ;> - vector-2d (vector-2d)
    ;> - x (number) The x position of the cell
    ;> - y (number) The y position of the cell
    ;> - value (object) The new value of the cell
    (define-syntax vector-2d-set!
      (syntax-rules ()
        ((_ vector-2d x y value)
         (vector-set! (vector-ref (vector-2d-cells-get vector-2d) y) x value))))


    ;> ### vector-2d-for-each
    ;>
    ;> Arguments:
    ;> - procedure (lambda ((object) (number) (number)) ...) Procedure that takes as argument a cells value, x and y
    ;> - v2d (vector-2d) The vector you want to loop trough
    ;>
    ;> Only works on vector 2d's where width and length are the same.
    ;> If they are not the same then it loops only to the shortest.
    (define vector-2d-for-each
      (lambda (proc v2d)
        (letrec*
          ((width (vector-2d-width-get v2d))
           (height (vector-2d-height-get v2d))
           (x -1)
           (y 0)
           (looper
             (lambda ()
               (set! x (+ x 1))
               (if (>= x width)
                 (begin
                   (set! x 0)
                   (set! y (+ y 1))))
               (if (<= y (- height 1))
                 (begin
                   (apply proc (list (vector-2d-get v2d x y) x y))
                   (looper))))))
          (looper))))

    ;> ### vector-2d-display
    ;>
    ;> Arguments:
    ;> - vector-2d (vector-2d)
    (define vector-2d-display
      (lambda (vector-2d)
        (vector-for-each
          (lambda (line)
            (display line)
            (newline))
          (vector-2d-cells-get vector-2d))))))
