# Event



## Types


### event
Record that contains the event type and event data


## Procedures


### event?
Arguments
- object (object) Object you want to check if it is event type

Returns
 - (boolean) True if object is event type


### event-type-get
Arguments
- event (event)
  - 


### event-poll

Returns (list event...) Get all the events in the event-queue


