;> # Event
;>
(define-library
  (retropikzel event v0-1-0 main)
  (import (scheme base)
          (scheme write))
  (export event-poll
          event-make
          event?
          event-type-get
          event-data-get
          event-wait
          event-push
          event-clear)
  (begin

    ;> ## Types

    ;> ### event
    ;> Record that contains the event type and event data

    ;> ## Procedures

    ;> ### event-make
    ;> Arguments
    ;> - type (symbol) The type of the event
    ;> - data (list) The data of the event as a list
    ;>
    ;> Returns
    ;> - (event) Returns newly made event

    ;> ### event?
    ;> Arguments
    ;> - object (object) Object you want to check if it is event type
    ;>
    ;> Returns
    ;>  - (boolean) True if object is event type

    ;> ### event-type-get
    ;> Arguments
    ;> - event (event)
    ;>
    ;> Returns
    ;> - (synbol) The type of event
    (define-record-type event
      (event-make type data)
      event?
      (type event-type-get)
      (data event-data-get))

    (define events (vector))

    ;> ### event-poll
    ;>
    ;> Returns (list event...) Get all the events in the event-queue
    (define event-poll
      (lambda ()
        (let ((events-copy (vector-copy events)))
          (set! events (vector))
          events-copy)))

    (define event-wait
      (lambda ()
        (if (= (vector-length events) 0)
          (event-wait)
          (event-poll))))

    (define event-push
      (lambda (event)
        (if (not (event? event))
          (error "Pushed event must be of type event" event)
          (set! events (vector-append events (vector event))))))

    (define event-clear
      (lambda ()
        (set! events (vector))))))
