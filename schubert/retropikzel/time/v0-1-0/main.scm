(define-library
  (retropikzel time v0-1-0 main)
  (import (scheme base)
          (scheme write)
          (scheme time))
  (export time-as-milliseconds
          time-as-seconds
          time-update
          time-timeout
          time-interval)
  (begin


    (define-record-type timeout
      (make-timeout index time proc)
      timeout?
      (index timeout-index)
      (time timeout-time)
      (proc timeout-proc))

    (define-record-type interval
      (make-interval index time proc milliseconds)
      interval?
      (index interval-index)
      (time interval-time interval-time-set!)
      (proc interval-proc)
      (milliseconds interval-milliseconds))

    (define queue (vector))

    ;> ### time-as-seconds
    ;>
    ;> Get the current timestamp as seconds
    (define time-as-seconds
      (lambda ()
        (/ (current-jiffy) (jiffies-per-second))))

    ;> ### time-as-millisecons
    ;>
    ;> Get the current timestamp as milliseconds
    (define time-as-milliseconds
      (lambda ()
        (* (time-as-seconds) 1000)))


    ;> ### time-update
    ;>
    ;> Update the internal timer and run any timeouts or intervals
    ;>
    ;> You need to call this frequently to make the timeouts and intervals work
    (define time-update
      (lambda ()
        (let ((queue-copy (vector-copy queue))
              (current-time (time-as-milliseconds)))

          (set! queue (vector))

          (vector-for-each
            (lambda (item)
              (cond
                ((timeout? item)
                 (if (< (timeout-time item) current-time)
                   (apply (timeout-proc item) (list))
                   (set! queue (vector-append queue (vector item)))))
                ((interval? item)
                 (if (< (interval-time item) current-time)
                   (begin
                     (apply (interval-proc item) (list))
                     (interval-time-set! item (+ current-time
                                                 (interval-milliseconds item)))))
                 (set! queue (vector-append queue (vector item))))))
            queue-copy))))

    ;> ### time-timeout
    ;>
    ;> Arguments:
    ;>
    ;> - proc (procedure)
    ;>   - Procedure you want to be run after given time
    ;> - milliseconds (number)
    ;>   - Number of milliseconds after you want your procedure to be run
    ;>
    ;> Returns (number) Index of the timeout
    (define time-timeout
      (lambda (proc milliseconds)
        (let ((index (vector-length queue)))
          (set! queue
            (vector-append
              queue
              (vector (make-timeout index
                                    (+ (time-as-milliseconds) milliseconds)
                                    proc))))
          index)))

    ;> ### time-interval
    ;>
    ;> Arguments:
    ;>
    ;> - proc (procedure)
    ;>   - Procedure you want to be run on the interval
    ;> - milliseconds (number)
    ;>   - Length of the interval
    ;>
    ;> Returns (number) Index of the interval
    (define time-interval
      (lambda (proc milliseconds)
        (let ((index (vector-length queue)))
          (set! queue
            (vector-append
              queue
              (vector (make-interval index
                                     (+ (time-as-milliseconds) milliseconds)
                                     proc
                                     milliseconds))))
          index)))


    ;> ### time-stop
    ;>
    ;> Arguments:
    ;>
    ;> - index (number)
    ;>   - Index of the timeout or interval returned by either time-timeout or time-interval
    (define time-stop
      (lambda (index)
        (vector-set! queue index (list))))

    ))
