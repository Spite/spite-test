# Color
Color library


## Records


### color
Fields:
- red (number) Between 0-255
- green (number) Between 0-255
- blue (number) Between 0-255
- alpha (number) Between 0-255


## Procedures


### color-make
Arguments:
- red (number) Between 0-255
- green (number) Between 0-255
- blue (number) Between 0-255
- alpha (number) Between 0-255

Returns
- (color)


### color?
Arguments:
- object (object) Object you want to check if its color

Returns
- (boolean) Returns #t if object is color, #f otherwise


### color-red-get
Arguments:
- color (color) The color record you want to get the red value from

Returns:
- (number) The amount of red in this color


### color-red-set!
Arguments:
- color (color) The color record you want to set the red value of
- red (number) New value of red for this color


### color-green-get
Arguments:
- color (color) The color record you want to get the green value from

Returns:
- (number) The amount of green in this color


### color-green-set!
Arguments:
- color (color) The color record you want to set the green value of
- green (number) New value of green for this color


### color-blue-get
Arguments:
- color (color) The color record you want to get the blue value from

Returns:
- (number) The amount of blue in this color


### color-blue-set!
Arguments:
- color (color) The color record you want to set the blue value of
- blue (number) New value of blue for this color


### color-alpha-get
Arguments:
- color (color) The color record you want to get the alpha value from

Returns:
- (number) The amount of alpha in this color


### color-alpha-set!
Arguments:
- color (color) The color record you want to set the alpha value of
- alpha (number) New value of alpha for this color


### color->vector
Arguments:
- color (color) The color you want to make into vector

Returns
- (vector number number number number) Colors red, green, blue and alpha


