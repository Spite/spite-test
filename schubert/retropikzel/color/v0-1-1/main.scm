;> # Color
;> Color library
(define-library
  (retropikzel color v0-1-1 main)
  (import (scheme base))
  (export color-make
          color?
          color-red-get
          color-red-set!
          color-green-get
          color-green-set!
          color-blue-get
          color-blue-set!
          color-alpha-get
          color-alpha-set!
          color->vector)
  (begin

    ;> ## Records

    ;> ### color
    ;> Fields:
    ;> - red (number) Between 0-255
    ;> - green (number) Between 0-255
    ;> - blue (number) Between 0-255
    ;> - alpha (number) Between 0-255
    (define-record-type color
      (color-make red green blue alpha)
      color?
      (red color-red-get color-red-set!)
      (green color-green-get color-green-set!)
      (blue color-blue-get color-blue-set!)
      (alpha color-alpha-get color-alpha-set!))

    ;> ## Procedures

    ;> ### color-make
    ;> Arguments:
    ;> - red (number) Between 0-255
    ;> - green (number) Between 0-255
    ;> - blue (number) Between 0-255
    ;> - alpha (number) Between 0-255
    ;>
    ;> Returns
    ;> - (color)

    ;> ### color?
    ;> Arguments:
    ;> - object (object) Object you want to check if its color
    ;>
    ;> Returns
    ;> - (boolean) Returns #t if object is color, #f otherwise

    ;> ### color-red-get
    ;> Arguments:
    ;> - color (color) The color record you want to get the red value from
    ;>
    ;> Returns:
    ;> - (number) The amount of red in this color

    ;> ### color-red-set!
    ;> Arguments:
    ;> - color (color) The color record you want to set the red value of
    ;> - red (number) New value of red for this color

    ;> ### color-green-get
    ;> Arguments:
    ;> - color (color) The color record you want to get the green value from
    ;>
    ;> Returns:
    ;> - (number) The amount of green in this color

    ;> ### color-green-set!
    ;> Arguments:
    ;> - color (color) The color record you want to set the green value of
    ;> - green (number) New value of green for this color

    ;> ### color-blue-get
    ;> Arguments:
    ;> - color (color) The color record you want to get the blue value from
    ;>
    ;> Returns:
    ;> - (number) The amount of blue in this color

    ;> ### color-blue-set!
    ;> Arguments:
    ;> - color (color) The color record you want to set the blue value of
    ;> - blue (number) New value of blue for this color

    ;> ### color-alpha-get
    ;> Arguments:
    ;> - color (color) The color record you want to get the alpha value from
    ;>
    ;> Returns:
    ;> - (number) The amount of alpha in this color

    ;> ### color-alpha-set!
    ;> Arguments:
    ;> - color (color) The color record you want to set the alpha value of
    ;> - alpha (number) New value of alpha for this color


    ;> ### color->vector
    ;> Arguments:
    ;> - color (color) The color you want to make into vector
    ;>
    ;> Returns
    ;> - (vector number number number number) Colors red, green, blue and alpha
    (define color->vector
      (lambda (color)
        (vector (color-red-get color)
                (color-green-get color)
                (color-blue-get color)
                (color-alpha-get color))
        ))))
