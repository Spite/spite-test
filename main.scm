
(spite-window-resizing-allow)
(define display-event (lambda (event) (display event) (newline)))

(define handle-key-down
  (lambda (key)
    (write key)
    (newline)
    (cond
      ((string=? key "Space") (event-push (event-make 'test "Hello")))
      ((string=? key "W") (set! y (- y 10)))
      ((string=? key "A") (set! x (- x 10)))
      ((string=? key "S") (set! y (+ y 10)))
      ((string=? key "D") (set! x (+ x 10)))

      )))

(define handle-test
  (lambda (data)
    (display data)
    (newline)))

(define update
  (lambda (events)
    (vector-for-each
      (lambda (event)
        (let ((type (event-type-get event))
              (data (event-data-get event)))
          (cond
            ((equal? type 'quit) (emergency-exit))
            ((equal? type 'key-down)
             (write (assoc 'key data))
             (newline)
             (handle-key-down (cdr (assoc 'key data)))

             )
            ((equal? type 'test) (handle-test data)))
          ;(display type)
          ;(newline)
          ;(write (event-data-get event))
          ;(newline)

          )
        )
      events)))


(define draw
  (lambda ()
    (spite-image-draw test-image 100 100 1080 520)
    ;(spite-image-draw-slice test-image x y 32 32 0 0 32 32)
    ;(spite-text-draw "Hello world" 50 50 test-font black)
    ;(spite-point-draw test-point 25 black)
    ;(spite-line-draw test-line 5 black)
    (spite-rectangle-draw test-rectangle 5 black)
    ))



(spite-start update draw)
