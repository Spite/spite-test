#!/usr/bin/env sh

sash -r7 -L . -L ./schubert -e '(import (scheme load)) (load "init.scm") (load "main.scm")'
exit
#guile -q --no-auto-compile --r7rs -L $(pwd) -L ./schubert -s main.scm
#exit
java \
    --add-exports java.base/jdk.internal.foreign.abi=ALL-UNNAMED \
    --add-exports java.base/jdk.internal.foreign.layout=ALL-UNNAMED \
    --add-exports java.base/jdk.internal.foreign=ALL-UNNAMED \
    --enable-native-access=ALL-UNNAMED \
    --enable-preview \
    -jar kawa.jar \
    --r7rs \
    --full-tailcalls \
    -Dkawa.import.path=".:./schubert" \
    -e '(load "init.scm") (load "main.scm")'
