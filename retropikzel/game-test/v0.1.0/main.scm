(define-library
  (retropikzel game-test v0.1.0 main)
  (import (scheme base)
          (scheme write)
          (retropikzel pffi v0.1.0 main))
  (export init)
  (begin

    (define sdl2 #f)
    (define window #f)
    (define renderer #f)

    (define allegro #f)

    (define init
      (lambda ()

        (set! sdl2 (pffi-shared-object-auto-load "SDL2"))
        (pffi-call sdl2 'SDL_Init 'int '((int . 32)))

        (set! window (pffi-call sdl2
                                'SDL_CreateWindow
                                'pointer
                                (list (cons 'pointer (pffi-string->pointer "Testing pffi"))
                                      (cons 'int 1)
                                      (cons 'int 1)
                                      (cons 'int 400)
                                      (cons 'int 400)
                                      (cons 'int 4))))

        (set! renderer (pffi-call sdl2
                                  'SDL_CreateRenderer
                                  'pointer
                                  (list (cons 'pointer window)
                                        (cons 'int -1)
                                        (cons 'int 2))))

        (pffi-call sdl2 'SDL_RenderClear 'int (list (cons 'pointer renderer)))
        (pffi-call sdl2 'SDL_RenderPresent 'int (list (cons 'pointer renderer)))

        (pffi-call sdl2 'SDL_Delay 'void '((int . 2000)))))

    ))
