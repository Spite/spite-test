#!/bin/sh

racket -I r7rs --make -S $(pwd)/schubert -f init.scm -f main.scm
#-e '(import (scheme base) (scheme load)) (load "init.scm") (load "main.scm")'
